## История изменений

### Release 8.0.1-alpha01
- Версия SDK Pay 8.0.1-alpha01.
- Класс `ProductPurchaseResult` теперь не имеет подклассов и содержит только результат успешной покупки.
- Методы `Purchase` и `PurchaseTwoStep` теперь возвращают результат успешной покупки в `onSuccess` и любые ошибки покупки в `onFailure`.
- Добавлены классы ошибок `RuStorePaymentException`.

### Release 8.0.0
- Версия SDK Pay 8.0.0.
- Ключ `console_app_id_key` в манифесте заменён на `console_app_id_value`.
- Добавлена поддержка диплинков.
- Вместо метода `PurchaseOneStep` теперь используется метод `Purchase` с параметром `preferredPurchaseType`.

### Release 7.0.0
- Версия SDK Pay 7.0.0.
