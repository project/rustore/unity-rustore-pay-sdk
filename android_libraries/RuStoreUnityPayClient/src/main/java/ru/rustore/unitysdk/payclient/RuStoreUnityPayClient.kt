package ru.rustore.unitysdk.payclient

import android.content.Intent
import ru.rustore.sdk.pay.RuStorePayClient
import ru.rustore.sdk.pay.model.AppUserId
import ru.rustore.sdk.pay.model.DeveloperPayload
import ru.rustore.sdk.pay.model.OrderId
import ru.rustore.sdk.pay.model.PreferredPurchaseType
import ru.rustore.sdk.pay.model.ProductId
import ru.rustore.sdk.pay.model.ProductPurchaseParams
import ru.rustore.sdk.pay.model.ProductType
import ru.rustore.sdk.pay.model.PurchaseId
import ru.rustore.sdk.pay.model.Quantity
import ru.rustore.unitysdk.payclient.callbacks.CancelTwoStepPurchaseListener
import ru.rustore.unitysdk.payclient.callbacks.ConfirmTwoStepPurchaseListener
import ru.rustore.unitysdk.payclient.callbacks.ProductPurchaseResultListener
import ru.rustore.unitysdk.payclient.callbacks.ProductsResponseListener
import ru.rustore.unitysdk.payclient.callbacks.PurchaseAvailabilityListener
import ru.rustore.unitysdk.payclient.callbacks.PurchaseResponseListener
import ru.rustore.unitysdk.payclient.callbacks.PurchasesResponseListener

object RuStoreUnityPayClient {

	private val PreferredPurchaseTypeDefault = PreferredPurchaseType.ONE_STEP

	fun getPurchaseAvailability(listener: PurchaseAvailabilityListener) {
		RuStorePayClient.instance.getPurchaseInteractor().getPurchaseAvailability()
			.addOnSuccessListener { result -> listener.OnSuccess(result) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun getProducts(productsId: Array<String>, listener: ProductsResponseListener) {
		val ids = productsId.map { ProductId(it) }.toList()
		RuStorePayClient.instance.getProductInteractor().getProducts(ids)
			.addOnSuccessListener { result -> listener.OnSuccess(result) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun getPurchases(productType: String?, listener: PurchasesResponseListener) {
		val type = productType?.let {
			try {
				ProductType.valueOf(productType)
			} catch (e: IllegalArgumentException) {
				null
			}
		}
		RuStorePayClient.instance.getPurchaseInteractor().getPurchases(type)
			.addOnSuccessListener { result -> listener.OnSuccess(result) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun getPurchase(purchaseId: String, listener: PurchaseResponseListener) {
		RuStorePayClient.instance.getPurchaseInteractor().getPurchase(PurchaseId(purchaseId))
			.addOnSuccessListener { result -> listener.OnSuccess(result) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun purchase(productId: String, appUserId: String?, orderId: String?, quantity: Int, developerPayload: String?, preferredPurchaseType: String, listener: ProductPurchaseResultListener) {
		val params = ProductPurchaseParams(
			productId = ProductId(productId),
			appUserId = appUserId?.let{ AppUserId(it) },
			orderId = orderId?.let{ OrderId(it) },
			quantity = Quantity(quantity),
			developerPayload = developerPayload?.let { DeveloperPayload(it) }
		)
		val purchaseType = try {
			PreferredPurchaseType.valueOf(preferredPurchaseType)
		} catch (e: IllegalArgumentException) {
			PreferredPurchaseTypeDefault
		}

		RuStorePayClient.instance.getPurchaseInteractor().purchase(params, purchaseType)
			.addOnSuccessListener { result -> listener.OnSuccess(result) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun purchaseTwoStep(productId: String, appUserId: String?, orderId: String?, quantity: Int, developerPayload: String?, listener: ProductPurchaseResultListener) {
		RuStorePayClient.instance.getPurchaseInteractor().purchaseTwoStep(
			ProductPurchaseParams(
				productId = ProductId(productId),
				appUserId = appUserId?.let{ AppUserId(it) },
				orderId = orderId?.let{ OrderId(it) },
				quantity = Quantity(quantity),
				developerPayload = developerPayload?.let { DeveloperPayload(it) }
			))
			.addOnSuccessListener { result -> listener.OnSuccess(result) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun confirmTwoStepPurchase(purchaseId: String, developerPayload: String?, listener: ConfirmTwoStepPurchaseListener) {
		RuStorePayClient.instance.getPurchaseInteractor()
			.confirmTwoStepPurchase(
				purchaseId = PurchaseId(purchaseId),
				developerPayload = developerPayload?.let { DeveloperPayload(it) })
			.addOnSuccessListener { listener.OnSuccess() }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun cancelTwoStepPurchase(purchaseId: String, listener: CancelTwoStepPurchaseListener) {
		RuStorePayClient.instance.getPurchaseInteractor()
			.cancelTwoStepPurchase(PurchaseId(purchaseId))
			.addOnSuccessListener { listener.OnSuccess() }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun proceedIntent(intent: Intent) {
		RuStorePayClient.instance.getIntentInteractor().proceedIntent(intent)
	}
}
