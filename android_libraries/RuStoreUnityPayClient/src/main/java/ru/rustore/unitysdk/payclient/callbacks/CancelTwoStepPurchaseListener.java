package ru.rustore.unitysdk.payclient.callbacks;

public interface CancelTwoStepPurchaseListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess();
}
