package ru.rustore.unitysdk.payclient.callbacks;

public interface ConfirmTwoStepPurchaseListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess();
}
