package ru.rustore.unitysdk.payclient.callbacks;

import ru.rustore.sdk.pay.model.ProductPurchaseResult;

public interface ProductPurchaseResultListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess(ProductPurchaseResult response);
}
