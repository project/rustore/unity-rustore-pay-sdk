package ru.rustore.unitysdk.payclient.callbacks;

import java.util.List;

import ru.rustore.sdk.pay.model.Product;

public interface ProductsResponseListener {
    public void OnFailure(Throwable throwable);
    public void OnSuccess(List<Product> response);
}
