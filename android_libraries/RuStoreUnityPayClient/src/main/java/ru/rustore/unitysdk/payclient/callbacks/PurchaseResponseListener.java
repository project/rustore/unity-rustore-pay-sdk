package ru.rustore.unitysdk.payclient.callbacks;

import ru.rustore.sdk.pay.model.Purchase;

public interface PurchaseResponseListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess(Purchase response);
}
