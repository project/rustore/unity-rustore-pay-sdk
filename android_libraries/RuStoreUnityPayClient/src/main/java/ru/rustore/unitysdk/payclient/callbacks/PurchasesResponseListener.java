package ru.rustore.unitysdk.payclient.callbacks;

import java.util.List;
import ru.rustore.sdk.pay.model.Purchase;

public interface PurchasesResponseListener {

    public void OnFailure(Throwable throwable);
    public void OnSuccess(List<Purchase> response);
}
