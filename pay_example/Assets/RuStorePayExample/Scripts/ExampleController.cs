using System;
using UnityEngine;
using RuStore.PayExample.UI;
using RuStore.PayClient;
using UnityEngine.UI;
using RuStore.CoreClient;

namespace RuStore.PayExample {

    public class ExampleController : MonoBehaviour {

        [SerializeField]
        private string[] productsId;

        [SerializeField]
        private CardsView productsView;

        [SerializeField]
        private CardsView purchasesView;

        [SerializeField]
        private PurchaseMethodBox purchaseMethodBox;

        [SerializeField]
        private MessageBox messageBox;

        [SerializeField]
        private LoadingIndicator loadingIndicator;

        [SerializeField]
        private Text isRuStoreInstalledLabel;

        [SerializeField]
        private ProductTypeView productTypeView;

        private void Start() {
            ProductCardView.OnBuyProduct += ProductCardView_OnBuyProduct;

            PurchaseCardView.OnConfirmPurchase += PurchaseCardView_OnConfirmPurchase;
            PurchaseCardView.OnCancelPurchase += PurchaseCardView_OnCancelPurchase;
            PurchaseCardView.OnGetPurchase += PurchaseCardView_OnGetPurchase;

            productTypeView.onValueChangedEvent += ProductTypeView_onValueChangedEvent;

            var isRuStoreInstalled = RuStoreCoreClient.Instance.IsRuStoreInstalled();
            var message = isRuStoreInstalled ? "RuStore is installed [v]" : "RuStore is not installed [x]";
            isRuStoreInstalledLabel.text = message;
        }

        private void ProductTypeView_onValueChangedEvent(object sender, ProductType? e) => LoadPurchases();

        public void GetPurchaseAvailability() {
            loadingIndicator.Show();

            RuStorePayClient.Instance.GetPurchaseAvailability(
                onFailure: (error) => {
                    loadingIndicator.Hide();
                    OnError(error);
                },
                onSuccess: (result) => {
                    loadingIndicator.Hide();

                    if (result.isAvailable) {
                        messageBox.Show("Availability", "True");
                    }
                    else {
                        OnError(result.cause);
                    }
                });
        }

        public void GetProducts() {
            loadingIndicator.Show();

            var ids = Array.ConvertAll(productsId, p => new ProductId(p));

            RuStorePayClient.Instance.GetProducts(
                productsId: ids,
                onFailure: (error) => {
                    loadingIndicator.Hide();
                    OnError(error);
                },
                onSuccess: (result) => {
                    loadingIndicator.Hide();
                    productsView.SetData(result);
                });
        }

        private void ProductCardView_OnBuyProduct(object sender, EventArgs e) {

            var product = (sender as ICardView<Product>).GetData();

            Action onPreferredOneStep = () => {
                loadingIndicator.Show();
                var parameters = new ProductPurchaseParams(product.productId);
                RuStorePayClient.Instance.Purchase(
                    parameters: parameters,
                    preferredPurchaseType: PreferredPurchaseType.ONE_STEP,
                    onFailure: (error) => {
                        loadingIndicator.Hide();
                        OnError(error);
                    },
                    onSuccess: (result) => {
                        loadingIndicator.Hide();
                    });
            };

            Action onPreferredOTwoStep = () => {
                loadingIndicator.Show();
                var parameters = new ProductPurchaseParams(product.productId);
                RuStorePayClient.Instance.Purchase(
                    parameters: parameters,
                    preferredPurchaseType: PreferredPurchaseType.TWO_STEP,
                    onFailure: (error) => {
                        loadingIndicator.Hide();
                        OnError(error);
                    },
                    onSuccess: (result) => {
                        loadingIndicator.Hide();
                    });
            };

            Action onTwoStep = () => {
                loadingIndicator.Show();
                var parameters = new ProductPurchaseParams(product.productId);
                RuStorePayClient.Instance.PurchaseTwoStep(
                    parameters: parameters,
                    onFailure: (error) => {
                        loadingIndicator.Hide();
                        OnError(error);
                    },
                    onSuccess: (result) => {
                        loadingIndicator.Hide();
                    });
            };

            purchaseMethodBox?.Show(product.title.value, onPreferredOneStep, onPreferredOTwoStep, onTwoStep);
        }

        public void LoadPurchases() {
            loadingIndicator.Show();
            RuStorePayClient.Instance.GetPurchases(
                productType: productTypeView.GetState(),
                onFailure: (error) => {
                    loadingIndicator.Hide();
                    OnError(error);
                },
                onSuccess: (result) => {
                    loadingIndicator.Hide();
                    purchasesView.SetData(result);
                });
        }

        private void PurchaseCardView_OnGetPurchase(object sender, EventArgs e) {
            loadingIndicator.Show();

            var purchase = (sender as ICardView<Purchase>).GetData();

            RuStorePayClient.Instance.GetPurchase(
                purchaseId: purchase.purchaseId,
                onFailure: (error) => {
                    loadingIndicator.Hide();
                    OnError(error);
                },
                onSuccess: (response) => {
                    loadingIndicator.Hide();
                    messageBox.Show("Purchase", string.Format("Purchase id: {0}", response.purchaseId));
                });
        }

        private void PurchaseCardView_OnConfirmPurchase(object sender, EventArgs e) {
            loadingIndicator.Show();

            var purchase = (sender as ICardView<Purchase>).GetData();
            RuStorePayClient.Instance.ConfirmTwoStepPurchase(
                purchaseId: purchase.purchaseId,
                developerPayload: null,
                onFailure: (error) => {
                    loadingIndicator.Hide();
                    OnError(error);
                },
                onSuccess: () => {
                    loadingIndicator.Hide();
                    LoadPurchases();
                });
        }

        private void PurchaseCardView_OnCancelPurchase(object sender, EventArgs e) {
            loadingIndicator.Show();

            var purchase = (sender as ICardView<Purchase>).GetData();
            RuStorePayClient.Instance.CancelTwoStepPurchase(
                purchaseId: purchase.purchaseId,
                onFailure: (error) => {
                    loadingIndicator.Hide();
                    OnError(error);
                },
                onSuccess: () => {
                    loadingIndicator.Hide();
                    LoadPurchases();
                });
        }

        public void ShowToast(string message) {
            using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            using (AndroidJavaObject utils = new AndroidJavaObject("com.plugins.payexample.RuStorePayAndroidUtils")) {
                utils.Call("showToast", currentActivity, message);
            }
        }

        private void OnError(RuStoreError error) {
            messageBox.Show(error.name, error.description);

            Debug.LogErrorFormat("{0} : {1}", error.name, error.description);
        }
    }
}
