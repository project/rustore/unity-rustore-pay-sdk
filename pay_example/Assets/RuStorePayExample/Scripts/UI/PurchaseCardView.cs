using System;
using UnityEngine;
using UnityEngine.UI;
using RuStore.PayClient;

namespace RuStore.PayExample.UI {

    public class PurchaseCardView : MonoBehaviour, ICardView<Purchase> {

        [SerializeField]
        private Text purchaseId;

        [SerializeField]
        private Text invoiceId;

        [SerializeField]
        private Text productId;

        [SerializeField]
        private Text orderId;

        [SerializeField]
        private Text amount;

        [SerializeField]
        private Text time;

        [SerializeField]
        private Text status;

        public static event EventHandler OnConfirmPurchase;
        public static event EventHandler OnCancelPurchase;
        public static event EventHandler OnGetPurchase;

        private Purchase purchase = null;

        public void SetData(Purchase purchase) {
            this.purchase = purchase;

            if (purchaseId != null) purchaseId.text = purchase.purchaseId.value;
            if (invoiceId != null) invoiceId.text = purchase.invoiceId.value;
            if (productId != null) productId.text = purchase.productId.value;
            if (orderId != null) orderId.text = purchase.orderId?.value;
            if (amount != null) amount.text = purchase.amountLabel.value;
            if (time != null) time.text = purchase.purchaseTime?.ToString();
            if (status != null) status.text = purchase.status.ToString();
        }

        public Purchase GetData() => purchase;

        public void ConfirmPurchase() => OnConfirmPurchase?.Invoke(this, EventArgs.Empty);

        public void CancelPurchase() => OnCancelPurchase?.Invoke(this, EventArgs.Empty);

        public void GetPurchase() => OnGetPurchase?.Invoke(this, EventArgs.Empty);
    }
}
