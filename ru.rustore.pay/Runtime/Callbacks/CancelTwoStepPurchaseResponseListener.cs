using RuStore.Internal;
using System;

namespace RuStore.PayClient.Internal {

    public class CancelTwoStepPurchaseResponseListener : SimpleResponseListener {

        private const string javaClassName = "ru.rustore.unitysdk.payclient.callbacks.CancelTwoStepPurchaseListener";

        public CancelTwoStepPurchaseResponseListener(Action<RuStoreError> onFailure, Action onSuccess) : base(javaClassName, onFailure, onSuccess) {
        }
    }
}
