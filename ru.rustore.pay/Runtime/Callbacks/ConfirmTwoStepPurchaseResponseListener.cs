using RuStore.Internal;
using System;

namespace RuStore.PayClient.Internal {

    public class ConfirmTwoStepPurchaseResponseListener : SimpleResponseListener {

        private const string javaClassName = "ru.rustore.unitysdk.payclient.callbacks.ConfirmTwoStepPurchaseListener";

        public ConfirmTwoStepPurchaseResponseListener(Action<RuStoreError> onFailure, Action onSuccess) : base(javaClassName, onFailure, onSuccess) {
        }
    }
}
