using RuStore.Internal;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace RuStore.PayClient.Internal {

    public class PurchaseResponseListener : ResponseListener<Purchase> {

        private const string javaClassName = "ru.rustore.unitysdk.payclient.callbacks.PurchaseResponseListener";

        public PurchaseResponseListener(Action<RuStoreError> onFailure, Action<Purchase> onSuccess) : base(javaClassName, onFailure, onSuccess) {
        }

        protected override Purchase ConvertResponse(AndroidJavaObject responseObject) =>
            responseObject != null ? DataConverter.ConvertPurchase(responseObject) : null;
    }
}
