using UnityEngine;
using System;

namespace RuStore.PayClient.Internal {

    public static partial class DataConverter {

        public static Product ConvertProduct(AndroidJavaObject obj) {
            if (obj == null) return null;

            var amountLabel = obj.Get<AndroidJavaObject>("amountLabel").Get<string>("value");
            var currency = obj.Get<AndroidJavaObject>("currency").Get<string>("value");
            var description = obj.Get<AndroidJavaObject>("description")?.Get<string>("value");
            var imageUrl = obj.Get<AndroidJavaObject>("imageUrl").Get<string>("value");
            var price = obj.Get<AndroidJavaObject>("price")?.Get<int>("value");
            var productId = obj.Get<AndroidJavaObject>("productId").Get<string>("value");
            var promoImageUrl = obj.Get<AndroidJavaObject>("promoImageUrl")?.Get<string>("value");
            var title = obj.Get<AndroidJavaObject>("title").Get<string>("value");
            var type = (ProductType)ConvertEnum<ProductType>(obj.Get<AndroidJavaObject>("type"));
            
            var product = new Product(
                new AmountLabel(amountLabel),
                new Currency(currency),
                description != null ? new Description(description) : null,
                new Url(imageUrl),
                price != null ? new Price((int)price) : null,
                new ProductId(productId),
                promoImageUrl != null ? new Url(imageUrl) : null,
                new Title(title),
                type
            );

            return product;
        }

        public static Purchase ConvertPurchase(AndroidJavaObject obj) {
            if (obj == null) return null;

            var amountLabel = obj.Get<AndroidJavaObject>("amountLabel").Get<string>("value");
            var currency = obj.Get<AndroidJavaObject>("currency").Get<string>("value");
            var description = obj.Get<AndroidJavaObject>("description").Get<string>("value");
            var developerPayload = obj.Get<AndroidJavaObject>("developerPayload")?.Get<string>("value");
            var invoiceId = obj.Get<AndroidJavaObject>("invoiceId").Get<string>("value");
            var orderId = obj.Get<AndroidJavaObject>("orderId")?.Get<string>("value");
            var price = obj.Get<AndroidJavaObject>("price").Get<int>("value");
            var productId = obj.Get<AndroidJavaObject>("productId").Get<string>("value");
            var productType = (ProductType)ConvertEnum<ProductType>(obj.Get<AndroidJavaObject>("productType"));
            var purchaseId = obj.Get<AndroidJavaObject>("purchaseId").Get<string>("value");
            var purchaseTime = ConvertDateTime(obj.Get<AndroidJavaObject>("purchaseTime"));
            var purchaseType = (PurchaseType)ConvertEnum<PurchaseType>(obj.Get<AndroidJavaObject>("purchaseType"));
            var quantity = obj.Get<AndroidJavaObject>("quantity").Get<int>("value");
            var status = (PurchaseStatus)ConvertEnum<PurchaseStatus>(obj.Get<AndroidJavaObject>("status"));
            var sandbox = obj.Get<bool>("sandbox");

            var purchase = new Purchase(
                new AmountLabel(amountLabel),
                new Currency(currency),
                new Description(description),
                developerPayload != null ? new DeveloperPayload(developerPayload) : null,
                invoiceId != null ? new InvoiceId(invoiceId) : null,
                new OrderId(orderId),
                new Price(price),
                new ProductId(productId),
                productType,
                new PurchaseId(purchaseId),
                purchaseTime,
                purchaseType,
                new Quantity(quantity),
                status,
                sandbox
            );

            return purchase;
        }

        public static ProductPurchaseResult ConvertProductPurchaseResult(AndroidJavaObject obj) {
            var invoiceId = obj.Get<AndroidJavaObject>("invoiceId").Get<string>("value");
            var orderId = obj.Get<AndroidJavaObject>("orderId")?.Get<string>("value");
            var productId = obj.Get<AndroidJavaObject>("productId").Get<string>("value");
            var purchaseId = obj.Get<AndroidJavaObject>("purchaseId").Get<string>("value");
            var purchaseType = (PurchaseType)ConvertEnum<PurchaseType>(obj.Get<AndroidJavaObject>("purchaseType"));
            var sandbox = obj.Get<bool>("sandbox");

            return new ProductPurchaseResult(
                new InvoiceId(invoiceId),
                orderId != null ? new OrderId(orderId) : null,
                new ProductId(productId),
                new PurchaseId(purchaseId),
                purchaseType,
                sandbox
            );
        }

        public static T? ConvertEnum<T>(AndroidJavaObject obj) where T : struct {
            Type type = typeof(T);
            string strValue = obj?.Call<string>("toString");
            object enumValue;

            return Enum.TryParse(type, strValue, true, out enumValue) ? (T?)enumValue : null;
        }

        public static DateTime? ConvertDateTime(AndroidJavaObject obj) {
            DateTime? dateTime = null;
            if (obj != null) {
                long time = obj.Call<long>("getTime");
                dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(time);
            }

            return dateTime;
        }
    }
}
