﻿using RuStore.Internal;
using UnityEngine;

namespace RuStore.PayClient.Internal {

    public static partial class DataConverter {

        public static RuStorePaymentException ConvertExceptionClasses(AndroidJavaObject obj) {
            var resultType = "";
            if (obj != null) {
                using (var javaClass = obj.Call<AndroidJavaObject>("getClass")) {
                    var className = javaClass.Call<string>("getName").Split('$');
                    resultType = className[className.Length - 1];
                }
            }

            switch (resultType) {
                case "ApplicationSchemeWasNotProvided":
                    return ConvertApplicationSchemeWasNotProvided(obj);

                case "EmptyPaymentTokenException":
                    return ConvertEmptyPaymentTokenException(obj);

                case "ProductPurchaseCancelled":
                    return ConvertProductPurchaseCancelled(obj);

                case "ProductPurchaseException":
                    return ConvertProductPurchaseException(obj);

                case "RuStorePayClientAlreadyExist":
                    return ConvertRuStorePayClientAlreadyExist(obj);

                case "RuStorePayClientNotCreated":
                    return ConvertRuStorePayClientNotCreated(obj);

                case "RuStorePayInvalidActivePurchase":
                    return ConvertRuStorePayInvalidActivePurchase(obj);

                case "RuStorePayInvalidConsoleAppId":
                    return ConvertRuStorePayInvalidConsoleAppId(obj);

                case "RuStorePaySignatureException":
                    return ConvertRuStorePaySignatureException(obj);

                case "RustorePaymentCommonException":
                    return ConvertRustorePaymentCommonException(obj);

                case "RustorePaymentNetworkException":
                    return ConvertRuStorePaymentNetworkException(obj);

                default:
                    return ConvertRuStorePaymentException(obj);
            }
        }

        public static RuStorePaymentException ConvertRuStorePaymentException(AndroidJavaObject obj) {
            var (name, message, cause) = ExtractNameMessageCause(obj);

            return new RuStorePaymentException(name, message, cause);
        }

        public static RuStorePaymentException.ApplicationSchemeWasNotProvided ConvertApplicationSchemeWasNotProvided(AndroidJavaObject obj) =>
            ConvertRuStorePaymentException(obj) as RuStorePaymentException.ApplicationSchemeWasNotProvided;

        public static RuStorePaymentException.EmptyPaymentTokenException ConvertEmptyPaymentTokenException(AndroidJavaObject obj) =>
            ConvertRuStorePaymentException(obj) as RuStorePaymentException.EmptyPaymentTokenException;

        public static RuStorePaymentException.ProductPurchaseCancelled ConvertProductPurchaseCancelled(AndroidJavaObject obj) {
            var (name, message, cause) = ExtractNameMessageCause(obj);
            var purchaseId = obj.Get<AndroidJavaObject>("purchaseId")?.Get<string>("value");
            var purchaseType = ConvertEnum<PurchaseType>(obj.Get<AndroidJavaObject>("purchaseType"));

            return new RuStorePaymentException.ProductPurchaseCancelled(
                name,
                message,
                cause,
                purchaseId != null ? new PurchaseId(purchaseId) : null,
                purchaseType
            );
        }

        public static RuStorePaymentException.ProductPurchaseException ConvertProductPurchaseException(AndroidJavaObject obj) {
            var (name, message, cause) = ExtractNameMessageCause(obj);
            var invoiceId = obj.Get<AndroidJavaObject>("invoiceId")?.Get<string>("value");
            var orderId = obj.Get<AndroidJavaObject>("orderId")?.Get<string>("value");
            var productId = obj.Get<AndroidJavaObject>("productId")?.Get<string>("value");
            var purchaseId = obj.Get<AndroidJavaObject>("purchaseId")?.Get<string>("value");
            var purchaseType = ConvertEnum<PurchaseType>(obj.Get<AndroidJavaObject>("purchaseType"));
            var quantity = obj.Get<AndroidJavaObject>("quantity")?.Get<int>("value");

            return new RuStorePaymentException.ProductPurchaseException(
                name,
                message,
                cause,
                invoiceId != null ? new InvoiceId(invoiceId) : null,
                orderId != null ? new OrderId(orderId) : null,
                productId != null ? new ProductId(productId) : null,
                purchaseId != null ? new PurchaseId(purchaseId) : null,
                purchaseType,
                quantity != null ? new Quantity((int)quantity) : null
            );
        }

        public static RuStorePaymentException.RuStorePayClientAlreadyExist ConvertRuStorePayClientAlreadyExist(AndroidJavaObject obj) =>
            ConvertRuStorePaymentException(obj) as RuStorePaymentException.RuStorePayClientAlreadyExist;

        public static RuStorePaymentException.RuStorePayClientNotCreated ConvertRuStorePayClientNotCreated(AndroidJavaObject obj) =>
            ConvertRuStorePaymentException(obj) as RuStorePaymentException.RuStorePayClientNotCreated;

        public static RuStorePaymentException.RuStorePayInvalidActivePurchase ConvertRuStorePayInvalidActivePurchase(AndroidJavaObject obj) =>
            ConvertRuStorePaymentException(obj) as RuStorePaymentException.RuStorePayInvalidActivePurchase;

        public static RuStorePaymentException.RuStorePayInvalidConsoleAppId ConvertRuStorePayInvalidConsoleAppId(AndroidJavaObject obj) =>
            ConvertRuStorePaymentException(obj) as RuStorePaymentException.RuStorePayInvalidConsoleAppId;

        public static RuStorePaymentException.RuStorePaySignatureException ConvertRuStorePaySignatureException(AndroidJavaObject obj) =>
            ConvertRuStorePaymentException(obj) as RuStorePaymentException.RuStorePaySignatureException;

        public static RuStorePaymentException.RuStorePaymentCommonException ConvertRustorePaymentCommonException(AndroidJavaObject obj) =>
            ConvertRuStorePaymentException(obj) as RuStorePaymentException.RuStorePaymentCommonException;

        public static RuStorePaymentException.RuStorePaymentNetworkException ConvertRuStorePaymentNetworkException(AndroidJavaObject obj) {
            var code = obj.Get<int>("code");
            var (name, message, cause) = ExtractNameMessageCause(obj);
            
            return new RuStorePaymentException.RuStorePaymentNetworkException(code, name, message, cause);
        }

        private static (string name, string message, RuStoreError cause) ExtractNameMessageCause(AndroidJavaObject obj) {

            var errorJavaClass = obj.Call<AndroidJavaObject>("getClass");
            var name = errorJavaClass.Call<string>("getSimpleName");
            var message = obj.Get<string>("message");

            var causeObject = obj.Get<AndroidJavaObject>("cause");
            RuStoreError cause = causeObject != null
                ? ErrorDataConverter.ConvertError(causeObject)
                : null;

            return (name, message, cause);
        }
    }
}
