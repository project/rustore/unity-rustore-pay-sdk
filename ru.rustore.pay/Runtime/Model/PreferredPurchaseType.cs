namespace RuStore.PayClient {

    /// <summary>
    /// Предпочитаемый тип покупки.
    /// </summary>
    public enum PreferredPurchaseType {

        /// <summary>
        /// Одностадийная оплата.
        /// </summary>
        ONE_STEP,

        /// <summary>
        /// Двухстадийная оплата.
        /// </summary>
        TWO_STEP
    }
}
