namespace RuStore.PayClient {

    /// <summary>
    /// Веб-ссылка.
    /// </summary>
    public class Url : BaseValue<string> {
        
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="value">Значение.</param>
        public Url(string value) : base(value) { }
    }
}
